angular.module('calendarioFlor').component('contentHeader', {
    bindings:{
        frase1: '@',
        frase2: '@',
    },
    template: `
    <section class="content-header">
        <p> {{ $ctrl.frase1}} <br>
            {{ $ctrl.frase2}}
        </p>
    </section>
    `
})