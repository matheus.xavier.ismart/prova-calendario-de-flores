angular.module('calendarioFlor').config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider){
        $stateProvider.state('home', {
            url:"/home",
            templateUrl: "home/home.html"
        }).state('cadastro',{
            url:"/cadastro",
            templateUrl:"cadastro/cadastro.html"
        })

        $urlRouterProvider.otherwise('/home')
    }
])