const mongoose = require('mongoose')
module.exports = mongoose.connect('mongodb://localhost/db_incluirtecnologia', {
    useMongoClient: true,
    useNewUrlParser: true,
    useUnifiedTopology: true
})
