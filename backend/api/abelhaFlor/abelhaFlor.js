const restful = require('node-restful')
const abelhaFlor = require('./abelhaFlorService')
const mongoose = restful.mongoose

const abelhaSchema = new mongoose.Schema({
    nome: {type: String, required: true},
    especie: {type: String, required: true}
})

const florSchema = new mongoose.Schema({
    nome: {type: String, required: true},
    especie: {type: String, required: true},
    descricao: {type: String, required: false},
    mes: {type: String, required: true},
    //abelha: {type: [abelhaSchema], required: true},
    abelha: [abelhaSchema],
    imagem: {data: Buffer, contentTyoe: String, required: false}
})


module.exports = restful.model('flores', florSchema)
//module.exports = restful.model('abelhas', abelhaSchema)
