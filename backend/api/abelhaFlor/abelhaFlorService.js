const abelhaFlor = require('./abelhaFlor')

abelhaFlor.methods(['get', 'post', 'put', 'delete'])
abelhaFlor.updateOptions({new: true, runValidators: true})

abelhaFlor.route('count', function(req, res, next) {
    abelhaFlor.count(function(error, value){
        if(error){
            res.status(500).json({errors: [error]})
        }else{
            res.json({value})
        }
    })
})

module.exports = abelhaFlor